import os
import inspect
import sys


class Config(object):
    """
    Configuration base, for all environments.
    """
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///application.db'
    BOOTSTRAP_FONTAWESOME = True
    SECRET_KEY = "MINHACHAVESECRETA"
    CSRF_ENABLED = True
    LOGGER_NAME = "ShoppingList"
    HOST_ADDRESS = "zbox"
    PORT = 5000
    LOG_PATH = "log"
    # Get your reCaptche key on: https://www.google.com/recaptcha/admin/create
    #RECAPTCHA_PUBLIC_KEY = "6LffFNwSAAAAAFcWVy__EnOCsNZcG2fVHFjTBvRP"
    #RECAPTCHA_PRIVATE_KEY = "6LffFNwSAAAAAO7UURCGI7qQ811SOSZlgU69rvv7"

    def write(self, path):
        cfg = os.path.join(path, 'config.cfg')
        start = os.path.join(path, 'start.sh')
        db = os.path.join(path, 'database/application.db')
        db = os.path.abspath(db)
        log = os.path.join(path, 'log')
        self.LOG_PATH = os.path.abspath(log)
        self.SQLALCHEMY_DATABASE_URI = 'sqlite:///{0}'.format(db)
        with open(cfg, 'w') as f:
            f.write("############################################\n")
            f.write("#           Sample Config File              \n")
            f.write("############################################\n")
            for p, v in inspect.getmembers(self):
                print p
                if not p.startswith('__') and not inspect.ismethod(getattr(self, p)):
                    if type(getattr(self, p)) is str:
                        f.write('{0} = "{1}"\n'.format(p, v))
                    else:
                        f.write('{0} = {1}\n'.format(p, v))
        conf_path = os.path.abspath(cfg)
        print ('build shell script')
        with open(start, 'w') as f:
            f.write('#!/bin/bash\n\n')
            f.write('export SL_CONFIG_PATH={0}\n'.format(conf_path))
            f.write('\n\n# Sample gunicorn start\n')
            f.write('gunicorn -w 4 -b 0.0.0.0:5000 ShoppingList.ShoppingList:app \n')


class Distributet(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///database/application.db'
    LOG_PATH = "log"


def conf():
    base_path = sys.argv[1]
    print os.path.abspath('Start build config')
    os.makedirs('{0}/log'.format(base_path))
    os.makedirs('{0}/database'.format(base_path))
    sc = Distributet()
    conf_path = '{0}/config.cfg'.format(base_path)
    sc.write(base_path)

    os.environ['SL_CONFIG_PATH'] = os.path.abspath(conf_path)
    # db = os.path.join(base_path, 'database/application.db')
    # db = os.path.abspath(db)
    
    # print db
    # conn = sqlite3.connect(db)
    # conn.close()

    from ShoppingList.models import db
    db.create_all()


if __name__ == "__main__":
    conf()
