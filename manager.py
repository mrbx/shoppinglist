from flask_script import Manager
from flask_migrate import MigrateCommand
from ShoppingList import app
from ShoppingList.models import db
from ShoppingList.configuration import Distributet
import os
from configobj import ConfigObj


manager = Manager(app)


@manager.command
def initDB():
    "initiate the database"
    db.create_all()
    print ("db createt")



def conf():
    os.makedirs('ShoppingList/log')
    os.makedirs('ShoppingList/databse')
    sc = Distributet()
    sc.write('ShoppingList/config.cfg')
    
                      

manager.add_command('db', MigrateCommand)

    
if __name__ == '__main__':
    manager.run()
