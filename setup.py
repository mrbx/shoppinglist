from setuptools import setup


setup(
    name='Shopping_List_Home_Manager',
    version='0.6.0',
    long_description=__doc__,
    packages=['ShoppingList'],
    scripts=['run.py', 'manager.py', 'setupScript.py'],


    entry_points={
        'console_scripts': ['slSetup=setupScript:conf']
    },
    include_package_data=True,
    zip_safe=False,
    install_requires=['packaging', 'appdirs', 'configobj', 'itsdangerous',
                      'flask', 'werkzeug', 'jinja2', 'click', ' webencodings',
                      'flask-restful', 'flask-sqlalchemy', 'SQLAlchemy-utils',
                      'flask-admin', 'flask-bootstrap', 'flask-migrate','flask-mail',
                      'Pillow', 'qrcode',
                      'WeasyPrint']

)
