import StringIO
import qrcode
from PIL import Image
from flask import send_file, jsonify
from flask import flash, redirect, url_for
from flask import render_template
from flask_admin.actions import action
from flask_admin.contrib.sqla import ModelView
from flask_restful import Resource, reqparse
from ShoppingList import log, admin, app, api, mail
from models import Product, ProductLocation, ProductCategory, ToPrint, Recipien, db
import time
import json
from weasyprint import HTML
from flask_mail import Message


class ProductView(ModelView):
    edit_template = 'product_edit.html'
    column_filters = ('name', 'unit', 'locations')
    form_columns = ('name', 'unit', 'prise', 'comment',
                    'locations', 'categories')

    @action('print', 'Print', 'Are you sure you wont to add this batches to print list')
    def action_print(self, ids):
        query = Product.query.filter(Product.id.in_(ids))
        count = 0
        update = 0
        for product in query.all():
            url = '{0}query/product/{1.id}'.format(buildBaseUrl(), product)

            toPrint = ToPrint.query.filter_by(url=url).first()
            if toPrint is None:
                toPrint = ToPrint()
                toPrint.name = product.name
                toPrint.labelType = 'product'
                toPrint.url = url
                db.session.add(toPrint)
                log.info(
                    'add {0.name} with url {1} to Print List'.format(product, url))
            else:
                toPrint.quiantity += 1
                log.info('update url {0} to {1} Print List'.format(
                    url, toPrint.quiantity))
                update += 1

            count += 1
        db.session.commit()
        flash('{0} Products addetd {1} updaqtet'.format(count, update))


class LocationView(ModelView):

    @action('print', 'Print', 'Are you sure you wont to add this batches to print list')
    def action_print(self, ids):
        query = ProductLocation.query.filter(ProductLocation.id.in_(ids))
        count = 0
        update = 0
        for location in query.all():
            url = '{0}query/location/{1.id}'.format(buildBaseUrl(), location)

            toPrint = ToPrint.query.filter_by(url=url).first()
            if toPrint is None:
                toPrint = ToPrint()
                toPrint.name = location.name
                toPrint.labelType = 'location'
                toPrint.url = url
                db.session.add(toPrint)
                log.info(
                    'add {0.name} with url {1} to Print List'.format(location, url))
            else:
                toPrint.quiantity += 1
                log.info('update url {0} to {1} Print List'.format(
                    url, toPrint.quiantity))
                update += 1

            count += 1
        db.session.commit()
        flash('{0} Locations added {1} updaqtet'.format(count, update))


class CategoryView(ModelView):

    @action('print', 'Print', 'Are you sure you wont to add this batches to print list')
    def action_print(self, ids):
        query = ProductCategory.query.filter(ProductCategory.id.in_(ids))
        count = 0
        update = 0
        for category in query.all():
            url = '{0}query/category/{1.id}'.format(buildBaseUrl(), category)

            toPrint = ToPrint.query.filter_by(url=url).first()
            if toPrint is None:
                toPrint = ToPrint()
                toPrint.name = category.name
                toPrint.labelType = 'category'
                toPrint.url = url
                db.session.add(toPrint)
                log.info(
                    'add {0.name} with url {1} to Print List'.format(category, url))
            else:
                toPrint.quiantity += 1
                log.info('update url {0} to {1} Print List'.format(
                    url, toPrint.quiantity))
                update += 1

            count += 1
        db.session.commit()
        flash('{0} Locations added {1} updaqtet'.format(count, update))


class PrintBatcheView(ModelView):
    can_create = False
    can_edit = False
    column_filters = ('name', 'labelType')
    column_list = ('quiantity', 'name', 'labelType')

    @action('print', 'Print', 'Are you sure you wont to add this batches to print list')
    def action_print(self, ids):
        query = ToPrint.query.filter(ToPrint.id.in_(ids))
        batches = []
        columns = 6

        # put in a list first to calculate the empty fields
        for batch in query.all():
            for i in xrange(batch.quiantity):
                batches.append((batch.name, buildQRImageUrl(batch.id)))

        if (batches.__len__() % columns) != 0:
            # empty batches
            a = batches.__len__()
            e = columns - (a % columns)
            for i in xrange(e):
                batches.append(('', buildQRImageUrl(-1)))
        log.debug('{0} batches to print'.format(batches.__len__()))
        rows_per_page = 8
        pages = batches.__len__() / (rows_per_page * columns) + 1
        batches_num = batches.__len__()
        codes_per_page = rows_per_page * columns
        page_range = []
        for p in xrange(pages):
            page_range.append(
                (p * rows_per_page, (p * rows_per_page) + rows_per_page - 1))
            log.debug('add page {0}/{1}'.format(p, pages))
        log.debug(page_range)
        if (page_range[-1][1] > batches.__len__() / columns):
            # calculate last page
            last_page_rows = batches_num - ((page_range[-1][0] - 1) * columns)
            last_page_rows = last_page_rows / columns
            pt = (page_range[-1][0], page_range[-1][0] + last_page_rows - 1)
            page_range[-1] = pt
        log.debug(page_range)
        return render_template('print_batches.html', batches=batches, columns=columns,
                               rows=batches.__len__() / columns, pages=page_range)


def buildBaseUrl():
    host = app.config['HOST_ADDRESS']
    port = app.config['PORT']
    return 'http://{0}:{1}/'.format(host, port)


@app.route("/")
def index():
    log.info('request')
    return redirect(url_for('admin.index'))


def serve_pil_image(img):
    img_io = StringIO.StringIO()
    img.save(img_io, 'JPEG', quality=70)
    img_io.seek(0)
    return send_file(img_io, mimetype='image/jpg')


@app.route('/qr/<int:t>/<int:print_id>')
def serverQR(t, print_id):
    toPrint = ToPrint.query.get(print_id)
    log.debug(toPrint.url)
    if toPrint is not None:
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=4,
            border=1)
        qr.add_data(toPrint.url)
        img = qr.make_image()
        log.debug(toPrint.url)
        log.debug(img.size[0])
    else:
        img = Image.new('RGB', (370, 370))
    return serve_pil_image(img)


def buildQRImageUrl(print_id):
    return '{0}qr/{1}/{2}'.format(buildBaseUrl(), int(time.time() * 1000000), print_id)


@app.route('/query/<string:qr_type>/<int:db_id>')
def serverProducts(qr_type, db_id):
    if qr_type == 'location':
        log.debug('query location')
        location = ProductLocation.query.get(db_id)
        items = []
        for pr in location.products:
            log.debug(pr.name)
            items.append(pr)
        return render_template('list_items.html', items=items, name=location.name,
                               t=int(time.time() * 1000000))
    elif qr_type == 'category':
        log.debug('query category')
        category = ProductCategory.query.get(db_id)
        items = []
        for pr in category.products:
            log.debug(pr.name)
            items.append(pr)
        return render_template('list_items.html', items=items, name=category.name,
                               t=int(time.time() * 1000000))
    elif qr_type == 'product':
        product = Product.query.get(db_id)
        return product.__repr__()
    elif qr_type == 'shoppinglist':
        query = Product.query.filter(Product.need > 0)
        return render_template('list_items.html', items=query, name='Shopping List',
                               t=int(time.time() * 1000000))


@app.route('/print/<string:pr_type>')
def printShoppingList(pr_type):
    query = Product.query.filter(Product.need > 0)
    if pr_type == 'html':
        return render_template('print_shopping.html', products=query)
    if pr_type == 'pdf':
        pdf_io = StringIO.StringIO()
        HTML('{0}print/html'.format(buildBaseUrl())).write_pdf(target=pdf_io)
        pdf_io.seek(0)
        return send_file(pdf_io, mimetype='application/pdf')


@app.route('/emailview/<int:mail_id>')
def emailList(mail_id):
    if mail_id == 0:
        query = Recipien.query
        return render_template('email_shopping_list.html', recipien=query)
    else:
        r = Recipien.query.get(mail_id)
        log.info('Send mail to {0} <{1}>'.format(r.name, r.email))
        msg = Message('Einkaufs liste', sender='ebahls@googlemail.com',
                      recipients=[r.email])

        msg.body = "Im anhang ist die einkaufsliste zu finden"
        pdf_io = StringIO.StringIO()
        HTML('{0}print/html'.format(buildBaseUrl())).write_pdf(target=pdf_io)
        pdf_io.seek(0)
        msg.attach('Einkaufsliste.pdf', 'application/pdf', pdf_io.read())
        mail.send(msg)
        return "gesendet"

# Restful API


parser = reqparse.RequestParser()
parser.add_argument('need')


class ShoppingItem(Resource):

    def get(self, product_id):
        log.debug('get shopping item from {0}'.format(product_id))
        product = Product.query.get(product_id)
        if product is not None:
            log.debug('get Product {0} json : {1}'.format(
                product.name, json.dumps(product.toDict())))
            return jsonify(product.toDict())
        else:
            return '', 400

    def delete(self, product_id):
        log.debug('delet shopping item from {0}'.format(product_id))
        product = Product.query.get(product_id)
        product.need = 0
        db.session.commit()
        return json.dumps(product.toDict())

    def put(self, product_id):
        args = parser.parse_args()
        log.debug(args)
        need = int(args['need'])
        product = Product.query.get(product_id)
        product.need = need
        db.session.commit()
        return product.toDict()


api.add_resource(ShoppingItem, '/api/<int:product_id>')

admin.add_view(ProductView(Product, db.session))
admin.add_view(LocationView(ProductLocation, db.session))
admin.add_view(CategoryView(ProductCategory, db.session))
admin.add_view(PrintBatcheView(ToPrint, db.session))
admin.add_view(ModelView(Recipien, db.session))
