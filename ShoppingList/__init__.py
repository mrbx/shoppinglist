from flask import Flask
import logging
from logging.handlers import RotatingFileHandler
import sys  # import sys package, if not already imported
from flask_bootstrap import Bootstrap
from flask_admin import Admin
from flask_restful import Api
from flask_mail import Mail
import os

reload(sys)
sys.setdefaultencoding('utf-8')


app = Flask(__name__)
if 'SL_CONFIG_PATH' in os.environ:
    print ('using cfg file')
    app.config.from_envvar('SL_CONFIG_PATH')
else:
    app.config.from_object('ShoppingList.configuration.DevelopmentConfig')

Bootstrap(app)

admin = Admin(app, name='ShoppingList', template_mode='bootstrap3')
api = Api(app)
mail = Mail(app)
log = app.logger

formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh = RotatingFileHandler('{0}/{1}.log'.format(app.config['LOG_PATH'],
                                              app.config['LOGGER_NAME']),
                         maxBytes=10000, backupCount=4)
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)


log.addHandler(fh)
# log.addHandler(cons)
log.info("Start")

from views import *
