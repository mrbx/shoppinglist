from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from ShoppingList import app
from sqlalchemy_utils import database_exists


db = SQLAlchemy(app)


migrate = Migrate(app, db)


prod_loc = db.Table('asc_prod_loc',
                    db.Column('location_id', db.Integer,
                              db.ForeignKey('ProductLocation.id')),
                    db.Column('product_id', db.Integer,
                              db.ForeignKey('Product.id')),

                    )

prod_cat = db.Table('asc_prod_cat',
                    db.Column('category_id', db.Integer,
                              db.ForeignKey('ProductCategory.id')),
                    db.Column('product_id', db.Integer,
                              db.ForeignKey('Product.id')),

                    )


class Recipien(db.Model):
    __tablename__ = 'Recipien'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    email = db.Column(db.String(120), unique=False)

    def __repr__(self):
        return self.name
    

    
class Product(db.Model):
    __tablename__ = 'Product'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    unit = db.Column(db.String(10))
    comment = db.Column(db.String(1024), default='')
    prise = db.Column(db.Float, default=0)
    need = db.Column(db.Integer, default=0)
    default_quantity = db.Column(db.Integer, default=1) 
    
    # location_id = db.Column(db.Integer, db.ForeignKey('ProductLocation.id'))
    category_id = db.Column(db.Integer, db.ForeignKey('ProductCategory.id'))
    locations = db.relationship('ProductLocation', secondary=prod_loc,
                                backref=db.backref('products', lazy='dynamic'))
    categories = db.relationship('ProductCategory', secondary=prod_cat,
                                 backref=db.backref('products', lazy='dynamic'))
    # productLocation = db.relationship('ProductLocation',
    # back_populates="products")

    def __repr__(self):
        return '{0} {1}'.format(self.unit, self.name)

    def toDict(self):
        return {'name':self.name, 'unit':self.unit, 'prise':self.prise, 'need':self.need}
        
    


class ProductLocation(db.Model):
    __tablename__ = 'ProductLocation'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)

    def __repr__(self):
        return self.name


class ProductCategory(db.Model):
    __tablename__ = 'ProductCategory'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)

    def __repr__(self):
        return self.name


class ShoppingList(db.Model):
    __tablename__ = 'ShoppingList'
    id = db.Column(db.Integer, primary_key=True)
    quantity = db.Column(db.Integer, default=1)
    comment = db.Column(db.String(220))
    product_id = db.Column(db.Integer, db.ForeignKey('Product.id'))
    product = db.relationship("Product", uselist=False)

    def __repr__(self):
        return '{0.quantity} {0.product.unit} {0.name}'.format(self)


class ToPrint(db.Model):

    _tablename__ = 'ToPrint'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    url = db.Column(db.String(250), unique=True)
    quiantity = db.Column(db.Integer, default=1)
    labelType = db.Column(db.String(10))

    def __repr__(self):
        return '{0.name}'.format(self)
